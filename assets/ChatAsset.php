<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-06-05 09:26:47
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-05 09:31:15
*/

namespace app\assets;

use yii\web\AssetBundle;

class ChatAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/chat.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
