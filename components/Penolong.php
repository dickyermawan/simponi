<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-05 18:08:34
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-18 18:21:20
*/

namespace app\components;

class Penolong {

	public static function label($nilaiLabel)
	{
		$xLabel = [
			'Menunggu' => '<span class="label label-primary" style="font-size: 1em; margin-bottom:3px; width:100%; display: inline-block;">'.$nilaiLabel.'</span>',
			'Diterima' => '<span class="label label-success" style="font-size: 1em; margin-bottom:3px; width:100%; display: inline-block;">'.$nilaiLabel.'</span>',
			'Tidak Diterima' => '<span class="label label-warning" style="font-size: 1em; margin-bottom:3px; width:100%; display: inline-block;">'.$nilaiLabel.'</span>',
		];
		return $xLabel[$nilaiLabel];
	}

	public static function activeChat($a, $b)
	{
		// return 'active';
		if($a==$b) return 'active';
	}

}