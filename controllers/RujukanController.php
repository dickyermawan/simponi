<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-11 17:37:35
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-21 10:07:46
*/

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use app\models\Pengguna;
use app\models\Rujukan;
use app\models\RujukanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

use yii\filters\AccessControl;
use app\components\AccessRule;
use app\models\User;


use ElephantIO\Client as Socket;
use ElephantIO\Engine\SocketIO\Version1X;

/**
 * RujukanController implements the CRUD actions for Rujukan model.
 */
class RujukanController extends Controller
{

    protected $socket;
    protected $pageSize;

    /**
     * Inisialisasi awal sebelum method dieksekusi.
     * @return void
     */
    public function init()
    {
        $this->socket = new Socket(new Version1X('127.0.0.1:3000'));
        $this->pageSize = 15;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => [
                    //admin
                    'monitoring', 'monitoring-partial','proses',

                    //user
                    'tambah','riwayat', 'riwayat-partial','cetak', 'cetak-tes'
                ],
                'rules' => [
                    [
                        'actions' => ['monitoring', 'proses', 'monitoring-partial'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                    [
                        'actions' => ['tambah', 'riwayat', 'riwayat-partial', 'cetak', 'cetak-tes'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_USER,
                        ],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'cetak' => ['POST'],
                ],
            ],
        ];
    }

    
    /**
     * Displays a single Rujukan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Rujukan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * Finds the Rujukan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rujukan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rujukan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    //================================================USER
    public function actionHapus($id)
    {
        $this->findModel($id)->delete();
        //untuk socket
        $this->socket->initialize();
        $this->socket->emit('hapus_rujukan', []);
        $this->socket->close();
        return $this->redirect(['riwayat']);
    }

    public function actionRiwayatPartial()
    {
        $searchModel = new RujukanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->identity->id);
        $dataProvider->pagination->pageSize=10;

        return $this->renderPartial('riwayat-partial', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }
    public function actionRiwayat()
    {
        $searchModel = new RujukanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->identity->id);
        $dataProvider->pagination->pageSize = 10;

        return $this->render('riwayat', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTambah()
    {
        $model = new Rujukan();

        if ($model->load(Yii::$app->request->post())) {
            date_default_timezone_set("Asia/Jakarta");
            $model->tgl_masuk = date('Y-m-d  H:i:s');

            $model->status = Rujukan::MENUNGGU;
// exit;
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', 'Data Rujukan berhasil dikirimkan, harap menunggu respon.');

                //untuk socket
                $this->socket->initialize();
                $this->socket->emit('tambah_rujukan', []);
                $this->socket->close();

                // return 'sukses save';
// exit;
                //NANTI DIAKTIFKAN LAGI, ini utk ngetes biar form terisi terus
                return $this->redirect(['rujukan/riwayat']);
            }else{
                echo 'GAGAL save';
            
            // else{ //UNTUK DEBUG, ELSE dari BERHASIL SAVE
                print_r($model);
            }
            // }
        }

        $model->asal_rujukan = Yii::$app->user->identity->id;
        $model->tujuan_rujukan = 2;

        $model->jenis = 'Ibu';
        // $model->jk = 'Perempuan';

        $model->tujuan_r = Pengguna::findOne($model->tujuan_rujukan)->nama_rs_puskesmas;
        $model->asal_r = Yii::$app->user->identity->nama_rs_puskesmas;

        return $this->render('tambah', [
            'model' => $model,
        ]);
    }

    public function actionUpdateJk()
    {
        return $this->renderPartial('update-jk',[
            'id' => $_POST['id']
        ]);
    }

    public function actionUpdateUmur()
    {
        return $this->renderPartial('update-umur',[
            'id' => $_POST['id']
        ]);
    }

    public function actionUpdateCaraBayar()
    {
        return $this->renderPartial('update-cara-bayar',[
            'id' => $_POST['id']
        ]);
    }



    //UNTUK CETAK HASIL RUJUKAN

    public function actionCetakTes()
    {
        $model = Rujukan::findOne(2);
// echo 'asd';
        // exit;

        return $this->render('_user-cetak-rujukan',['model'=>$model]);
    }

    public function actionCetak($id)
    {
        $model = $this->findModel($id);

        $content = $this->renderPartial('_user-cetak-rujukan',['model'=>$model]);

        date_default_timezone_set('Asia/Jakarta');
        $tanggal = Yii::$app->formatter->asDate(date('d M y'));
        $jam = date('H:i:s');
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_LEGAL, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // 'destination' => Pdf::DEST_BROWSER, 
            'destination' => Pdf::DEST_DOWNLOAD, 
            'content' => $content,  
            'filename' => 'Bukti Rujukan a.n '.$model->nama.' - '. $tanggal .'.pdf',
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.table > thead > tr > th,.table > tbody > tr > th,.table > tfoot > tr > th,.table > thead > tr > td,.table > tbody > tr > td,.table > tfoot > tr > td { padding: 3px; line-height: 1.42857143; vertical-align: top; border-top: 1px solid #dddddd;}',
            'methods' => [ 
                'SetHeader' => ['Dicetak Otomatis Oleh: SIMPONI||Pada: ' . $tanggal .', '. $jam],
                'SetFooter'=>['Hal. {PAGENO}'],
            ]

        ]);
        
        return $pdf->render(); 
    }





    // <==========================Akun RS==============================================================>
    public function actionUnreadCounter()
    {
        return Rujukan::find()->where(['status' => 'Menunggu'])->count();
    }
    public function actionMonitoring()
    {
        $searchModel = new RujukanSearch();
        $dataProvider = $searchModel->searchMonitoring(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;

        // print_r($dataProvider->getPagination());exit;

        return $this->render('rs-monitoring', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    //untuk render partial socket
    public function actionMonitoringPartial()
    {
        $searchModel = new RujukanSearch();
        $dataProvider = $searchModel->searchMonitoring(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize=10;
        // $pagination = $dataProvider->getPagination();
        // $pagination->route = 'rujukan/monitoring';
        // $dataProvider->pagination->route = 'rujukan/monitoring';

        // print_r($dataProvider->getPagination());exit;

        return $this->renderPartial('rs-monitoring-partial', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    //untuk notif di navbar
    public function actionMonitoringNotif()
    {
        $unreadMessages = \app\Models\Rujukan::find()->where(['status' => 'Menunggu'])->count();

        return $this->renderPartial('rs-monitoring-notif', [
            'unreadMessages' => $unreadMessages,
        ]);
    }

    public function actionProses($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'prosesRujukan';
        $model->asal_r = Pengguna::findOne($model->asal_rujukan)->nama_rs_puskesmas;
        $model->tujuan_r = Pengguna::findOne($model->tujuan_rujukan)->nama_rs_puskesmas;

        if ($model->load(Yii::$app->request->post()))
        {
            // print_r($model); 
            // exit;

            if($model->save()){

                //untuk socket
                $this->socket->initialize();
                $this->socket->emit('proses_rujukan', []);
                $this->socket->close();

                Yii::$app->getSession()->setFlash('success', 'Data Pasien berhasil diproses.');
                return $this->redirect(['rujukan/monitoring']);

                // return $this->redirect(['view', 'id' => $model->id]);
            }else{
                Yii::$app->getSession()->setFlash('danger', 'Data Pasien gagal diproses.');
                return $this->redirect(['rujukan/monitoring']);
            }
        }

        return $this->render('rs-proses', [
            'model' => $model,
        ]);
    }

    public function actionDetail()
    {
        $model = $this->findModel($_GET['id']);
        $model->asal_r = Pengguna::findOne($model->asal_rujukan)->nama_rs_puskesmas;
        $model->tujuan_r = Pengguna::findOne($model->tujuan_rujukan)->nama_rs_puskesmas;

        return $this->renderAjax('view',[
            'model' => $model
        ]);
    }
}
