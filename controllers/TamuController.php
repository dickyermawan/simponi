<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-04-22 16:44:10
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-08 01:07:25
*/

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class TamuController extends Controller
{
	public $layout = 'tema-tamu';

	public function actionMasuk()
	{

		if (!Yii::$app->user->isGuest) {
            // return $this->goBack();
            return $this->redirect(['site/index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

        	return $this->redirect(['site/index']);
            // return $this->goBack();
        }
		return $this->render('masuk',[
			'model' => $model
		]);
	}


	// =============================================UNTUK MODAL====================================================
    public function actionLupaPass()
    {
        return $this->renderAjax('lupa-pass');
    }
}
