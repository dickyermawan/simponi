<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-11 17:37:01
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-18 18:32:23
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "rujukan".
 *
 * @property int $id
 * @property string $tgl_masuk
 * @property string $jenis
 * @property string $nama
 * @property string $jk
 * @property string $tplahir
 * @property string $tglahir
 * @property string $umur
 * @property string $alamat
 * @property string $cara_bayar
 * @property string $nik
 * @property int $no_bpjs_jkd
 * @property int $asal_rujukan
 * @property int $tujuan_rujukan
 * @property string $alasan_rujukan
 * @property string $anamnesa
 * @property string $kesadaran
 * @property string $tekanan_darah
 * @property int $nadi
 * @property string $suhu
 * @property int $pernapasan
 * @property string $nyeri
 * @property string $pemeriksaan_fisik
 * @property string $pemeriksaan_penunjang
 * @property string $diagnosa
 * @property string $tindakan_yg_sdh_diberikan
 * @property string $info_balik
 * @property string $status
 */
class Rujukan extends \yii\db\ActiveRecord
{

    public $asal_r;
    public $tujuan_r;

    CONST MENUNGGU = 'Menunggu';
    CONST DITERIMA = 'Diterima';
    CONST TIDAK_DITERIMA = 'Tidak Diterima';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rujukan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_masuk', 'jenis', 'nama', 'asal_rujukan', 'tujuan_rujukan', 'alasan_rujukan', 'anamnesa', 'kesadaran', 'tekanan_darah', 'nadi', 'suhu', 'pernapasan', 'pemeriksaan_fisik', 'pemeriksaan_penunjang', 'diagnosa', 'tindakan_yg_sdh_diberikan', 'status'], 'required'],
            [['info_balik', 'status'], 'required', 'on' => 'prosesRujukan', 'message' => '{attribute} tidak boleh kosong.'],
            [['tgl_masuk', 'jk', 'tglahir', 'nyeri', 'tplahir', 'tglahir', 'umur', 'alamat', 'cara_bayar'], 'safe'],
            [['alamat', 'alasan_rujukan', 'anamnesa', 'pemeriksaan_fisik', 'pemeriksaan_penunjang', 'diagnosa', 'tindakan_yg_sdh_diberikan', 'info_balik'], 'string'],
            [['nik', 'no_bpjs_jkd', 'asal_rujukan', 'tujuan_rujukan', 'nadi', 'pernapasan'], 'integer'],
            [['jenis'], 'string', 'max' => 4],
            [['nama', 'tplahir', 'umur'], 'string', 'max' => 100],
            [['jk', 'nyeri', 'status'], 'string', 'max' => 25],
            [['cara_bayar', 'tekanan_darah', 'suhu'], 'string', 'max' => 10],
            [['kesadaran'], 'string', 'max' => 25],
            [['berat_bayi'], 'string', 'max' => 25]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tgl_masuk' => 'Tanggal Masuk',
            'jenis' => 'Jenis Pasien',
            'nama' => 'Nama',
            'jk' => 'Jenis Kelamin',
            'berat_bayi' => 'Berat Bayi',
            'tplahir' => 'Tempat Lahir',
            'tglahir' => 'Tanggal Lahir',
            'umur' => 'Umur',
            'alamat' => 'Alamat',
            'cara_bayar' => 'Cara Bayar',
            'nik' => 'NIK',
            'no_bpjs_jkd' => 'No BPJS/JKD',
            'asal_r' => 'Asal Rujukan',
            'asal_rujukan' => 'Asal Rujukan',
            'tujuan_r' => 'Tujuan Rujukan',
            'tujuan_rujukan' => 'Tujuan Rujukan',
            'alasan_rujukan' => 'Alasan Rujukan',
            'anamnesa' => 'Anamnesa',
            'kesadaran' => 'Kesadaran',
            'tekanan_darah' => 'Tekanan Darah',
            'nadi' => 'Nadi',
            'suhu' => 'Suhu',
            'pernapasan' => 'Pernapasan',
            'nyeri' => 'Nyeri',
            'pemeriksaan_fisik' => 'Pemeriksaan Fisik',
            'pemeriksaan_penunjang' => 'Pemeriksaan Penunjang',
            'diagnosa' => 'Diagnosa',
            'tindakan_yg_sdh_diberikan' => 'Tindakan Yang Sudah Diberikan',
            'info_balik' => 'Info Balik',
            'status' => 'Status',
        ];
    }

    public function attributeHints()
    {
        return array(
                // 'nama'=>'<span style="color:#e07734">*</span>',
                // 'tplahir'=>'<span style="color:#e07734">*</span>',
                // 'tglahir'=>'<span style="color:#e07734">*</span>',
                // 'umur'=>'<span style="color:#e07734">*</span>',
                // 'alamat'=>'<span style="color:#e07734">*</span>',
                // 'cara_bayar'=>'<span style="color:#e07734">*</span>',
                // 'tindakan_yg_sdh_diberikan'=>'<span style="color:#e07734">*</span>',
                // 'alasan_rujukan'=>'<span style="color:#e07734">*</span>',
                // 'diagnosa'=>'<span style="color:#e07734">*</span>',
        );
    }

    public function getRelasinamaasal()
    {
        return $this->hasOne(Pengguna::className(), ['id' => 'asal_rujukan']);
    }
    public function getAsal_rujukan_text() 
    {
        return $this->relasinamaasal->nama_rs_puskesmas;
    }


    // Start Untuk Laporan--------------------------------------------------------------
    public function getRelationAsalRujukan(){
        return $this->hasOne(Pengguna::className() ,['id' => 'asal_rujukan'])->select('nama_rs_puskesmas')->scalar();
    }
    public function getRelationTujuanRujukan(){
        return $this->hasOne(Pengguna::className() ,['id' => 'tujuan_rujukan'])->select('nama_rs_puskesmas')->scalar();
    }
    // End Untuk Laporan--------------------------------------------------------------




    public function getRelasinamatujuan()
    {
        return $this->hasOne(Pengguna::className(), ['id' => 'tujuan_rujukan']);
    }
    public function getTujuan_rujukan_text() 
    {
        return $this->relasinamatujuan->nama_rs_puskesmas;
    }
}
