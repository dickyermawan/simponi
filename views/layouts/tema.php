<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-04-22 14:28:21
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-21 13:55:09
*/

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Pjax;

use kartik\nav\NavX;
use kartik\icons\Icon;
Icon::map($this);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <style>
        /* Note: Try to remove the following lines to see the effect of CSS positioning */
        .affix {
          /*padding-bottom: 5px;*/
          top: 0;
          width: 100%;
          z-index: 9999 !important;
              position: fixed;
        }

        .affix + .container-fluid {
          padding-top: 82.5px;
        }

        .blink_text {
            animation:1s blinker linear infinite;
            -webkit-animation:1.3s blinker linear infinite;
            -moz-animation:1s blinker linear infinite;
        }
        @-moz-keyframes blinker {  
            0% { opacity: 1.0; }
            50% { opacity: 0.0; }
            100% { opacity: 1.0; }
        }
        @-webkit-keyframes blinker {  
            0% { opacity: 1.0; }
            50% { opacity: 0.0; }
            100% { opacity: 1.0; }
        }
        @keyframes blinker {  
            0% { opacity: 1.0; }
            50% { opacity: 0.0; }
            100% { opacity: 1.0; }
        }    
    
    /*    .nav-wrapper
        {
            min-height:108px;
        }*/

        audio { 
           display:none;
        }
    </style>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <link href="https://fonts.googleapis.com/css?family=Marck+Script|Signika+Negative" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<div class="wrap">
    
    <?php //Pjax::begin(['id' => 'main', 'timeout' => 3000]); ?>

    <!--<div class="container-fluid" style="background-color:#3aa7e6;color:#fff;height:108px;">-->
    <div class="container-fluid" style="background-color:#085886;color:#fff;height:108px;">
        <div class="col-md-offset-1 col-md-11" style="padding:10px">
            <div class="row">
                  <img style="float:left; padding-right:0px" width="60px" src="<?=Yii::getAlias('@web')?>/logo/riau.png"/>
                  <div class="content-heading" style="">
                    <h3 style="margin-bottom: 0px; padding-left:75px; color:white; font-family: 'Marck Script', cursive;"><strong>Rumah Sakit Umum Daerah Petala Bumi</strong></h3>
                      <span style="padding-left:15px; font-family: 'Poppins', sans-serif; font-size:18px;"><strong>SIMPONI</strong></span>
                  </div>
            </div>
        </div>
    </div>


<!-- <div class="nav-wrapper"> -->

    <?php
    NavBar::begin([
        // 'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-default affix untuk-font',
            // 'data-spy' => 'affix-top',
            'data-spy' => 'affix',
            'data-offset-top' => '106',
            'style' => 'border-radius:0'
        ],
    ]);

    $menuItems = [
        ['label' => Icon::show('home'). ' Beranda', 'url' => ['/site/index']],
    ];

    //Untuk Notifikasi Pesan
    $unreadPesan = \app\models\Chat::find()->where([
        'untuk' => Yii::$app->user->identity->id,
        'baca' => 0
    ])->count();

    if($unreadPesan){
        //udah berhasil bunyi, udah tes otomatis dgn penambahan
        $unreadPesan = '<span class="badge blink_text" style="margin-left: 8px;">'.$unreadPesan.'</span>';
        $unreadPesan .= '<audio autoplay><source src="'.Url::base().'/notif/pesan.ogg"></audio>';
    }else{
        $unreadPesan = '';
    }
    
    if(Yii::$app->user->identity->hak_akses==='super_admin')
    {
        array_push($menuItems,
            ['label' => 'Manajemen Pengguna', 
                'url' => ['/pengguna/index'],
                'active' => in_array(\Yii::$app->controller->id, ['pengguna']),
            ]
        );
    }elseif(Yii::$app->user->identity->hak_akses==='admin')
    {
        //Untuk Notifikasi
        $unreadMessages = \app\models\Rujukan::find()->where(['status' => 'Menunggu'])->count();

        //Untuk Suara Notif
        // if($unreadMessages && Yii::$app->controller->id.'/'.Yii::$app->controller->action->id != 'rujukan/proses'){
        //     //udah berhasil bunyi, belum tes otomatis dgn penambahan
        //     echo '<audio autoplay loop>
        //             <source src="'.Url::base().'/notif/AUD-20180506-WA0041.ogg">
        //           </audio>';
        // }

        // $unreadMessages = $unreadMessages ? '<span class="badge blink_text" style="margin-left: 8px;">'.$unreadMessages.'</span>' : '';

        if($unreadMessages){
            //udah berhasil bunyi, udah tes otomatis dgn penambahan
            $unreadMessages = '<span class="badge blink_text" style="margin-left: 8px;">'.$unreadMessages.'</span>';
            if(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id != 'rujukan/proses'){
                $unreadMessages .= '<audio autoplay loop>
                                        <source src="'.Url::base().'/notif/ambulan1.ogg">
                                    </audio>';
            }
        }else{
            $unreadMessages = '';
        }

        //cekcek
        array_push($menuItems,
            ['label' => 'Monitoring '.$unreadMessages, 
                'linkOptions' => ['class' => 'unread-monitoring'], 
                'url' => ['/rujukan/monitoring'],
                'active' => in_array($this->context->route, ['rujukan/monitoring']),
            ],
            ['label' => 'Laporan', 'url' => ['/laporan/index']],
            // ['label' => 'Pesan', 
            //     'url' => ['/pesan/chat']]
            ['label' => 'Pesan '.$unreadPesan,
            // ['label' => 'Pesan ',
                'linkOptions' => ['class' => 'unread-pesan'], 
                'url' => ['/pesan/chat'],
                'active' => in_array($this->context->route, ['/pesan/index']),
            ]
        );

    }elseif(Yii::$app->user->identity->hak_akses==='user')
    {
        //cekcek
        array_push($menuItems,
            ['label' => 'Rujukan',
                'url' => ['/rujukan/tambah'],
                'active' => in_array($this->context->route, ['rujukan/tambah']),
            ],
            ['label' => 'Riwayat Rujukan',
                'url' => ['/rujukan/riwayat'],
                'active' => in_array($this->context->route, ['rujukan/riwayat']),
            ],
            ['label' => 'Laporan', 'url' => ['/laporan/index']],
            // ['label' => 'Pesan', 
            //     'url' => ['/pesan/chat']]
            ['label' => 'Pesan '.$unreadPesan,
            // ['label' => 'Pesan ',
                'linkOptions' => ['class' => 'unread-pesan'], 
                'url' => ['/pesan/chat'],
                'active' => in_array($this->context->route, ['/pesan/index']),
            ]
        );
    }

    echo NavX::widget([
        'options' => ['class' => 'nav navbar-nav navbar-left'],
        'items' => $menuItems,
        'encodeLabels' => false
    ]);
    echo NavX::widget([
        'options' => ['class' => 'nav navbar-nav navbar-right'],
        'items' => [
            ['label' =>
                '<span class="fa fa-user"></span> &nbsp;' . Yii::$app->user->identity->nama_rs_puskesmas,
                'active' => in_array(\Yii::$app->controller->id, ['profil']),
                'items' => [
                    ['label' => Icon::show('pencil-square-o'). ' Edit',
                        'url' => ['/profil/edit'],
                        'active' => in_array(\Yii::$app->controller->id, ['akun'])
                    ],
                    '<li class="divider"></li>',
                    ['label' => Icon::show('sign-out'). ' Logout',
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ],
                ],
            ]
        ],
        'encodeLabels' => false
    ]);
    NavBar::end();
    ?>
<!-- </div> -->

    <div class="container-fluid untuk-font">
        <div style="padding: 1% 4% 3% 4%">
            <?= Breadcrumbs::widget([
                'homeLink' => ['label' => 'Beranda', 'url' => Yii::getAlias('@web')],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>

    <?php //Pjax::end(); ?>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 untuk-font" >
                <h4 class="title untuk-font">Alamat</h4>
                <p>
                    <strong>Jalan Dr. Soetomo No. 65, Sekip, LimaPuluh,<br>Kota Pekanbaru, Riau 28155</strong>
                </p>
                <p>
                    <?= Icon::show('clock-o')?> &nbsp;&nbsp;Buka 24 Jam<br>
                    <?= Icon::show('phone')?> &nbsp;&nbsp;(0761) 23024
                </p>
                <p></p>
            </div>
            <div class="col-sm-6">
                <h4 class="title untuk-font">Rumah Sakit Umum Daerah Petala Bumi</h4>
                <h4 class="title untuk-font">Provinsi Riau</h4>
            </div>
            </div>
        <hr>
        <div class="row text-center untuk-font"><a href="http://rsudpetalabumi.riau.go.id/" target="blank" style="color: #fff;">Copyright © RSUD Petala Bumi 2018</a></div>
    </div>  
</footer>



<?php
// $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.min.js');

// $this->registerJs('
//     let vHost = window.location.hostname

//     const socket = io(vHost+":3000")
//     const url = window.location.host

//     function replaceListTable() {
//         $.ajax({
//             url: url + "/../../rujukan/monitoring-partial",
//             method: "get",
//             success: function(data) {
//                 $(".guestbook-content").replaceWith(data)
//             }
//         })
//     }

//     // function replaceUnreadCounter() {
//     //     $.ajax({
//     //         url: url + "/../../rujukan/unread-monitoring",
//     //         method: "get",
//     //         success: function(data) {
//     //             $(".unread-monitoring").text(data)
//     //         }
//     //     })
//     // }
//     function replaceUnreadCounter() {
//         $.ajax({
//             url: url + "/../../rujukan/monitoring-notif",
//             method: "get",
//             success: function(data) {
//                 $(".unread-monitoring").replaceWith(data)
//             }
//         })
//     }

//     function replaceReadCounter() {
//         $.ajax({
//             url: url + "/../../rujukan/read-counter",
//             method: "get",
//             success: function(data) {
//                 $(".read-counter").text(data)
//             }
//         })
//     }

//     function checkAsRead(id, url) {
//         $.ajax({
//             url: url,
//             method: "get",
//             success: function(data) {}
//         })
//     }

//     socket.on("replace_guestbook", function(data) {
//         replaceListTable()
//     })

//     socket.on("replace_unread_counter", function() {
//         replaceUnreadCounter()
//     })

//     socket.on("replace_read_counter", function() {
//         replaceReadCounter()
//     })

//     $(".guestbook-list").on("click", ".view_message", function(e) {
//         e.preventDefault()
//         checkAsRead($(this).attr("value"), $(this).attr("href"))
//     })
// ');


// $this->registerJs('$(document).on("click", ".js-pjax", function(e){
//     e.preventDefault();
//     var $this = $(this);
//     var href = $this.attr("href");
//     var pjax_id = "w0";
//     $.pjax.reload({container:"#" + pjax_id, url:href});
//     return false;
// })');

?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
