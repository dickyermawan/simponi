<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-05 08:44:52
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-06 11:58:39
*/

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RujukanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rujukan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ibu_nama') ?>

    <?= $form->field($model, 'ibu_tplahir') ?>

    <?= $form->field($model, 'ibu_tglahir') ?>

    <?= $form->field($model, 'ibu_umur') ?>

    <?php // echo $form->field($model, 'ibu_alamat') ?>

    <?php // echo $form->field($model, 'ibu_carabayar') ?>

    <?php // echo $form->field($model, 'bayi_jk') ?>

    <?php // echo $form->field($model, 'bayi_nama') ?>

    <?php // echo $form->field($model, 'bayi_tplahir') ?>

    <?php // echo $form->field($model, 'bayi_tglahir') ?>

    <?php // echo $form->field($model, 'bayi_umur') ?>

    <?php // echo $form->field($model, 'bayi_alamat') ?>

    <?php // echo $form->field($model, 'bayi_carabayar') ?>

    <?php // echo $form->field($model, 'asal_rujukan') ?>

    <?php // echo $form->field($model, 'tujuan_rujukan') ?>

    <?php // echo $form->field($model, 'tindakan_ygdiberikan') ?>

    <?php // echo $form->field($model, 'alasan_rujukan') ?>

    <?php // echo $form->field($model, 'diagnosa') ?>

    <?php // echo $form->field($model, 'kesadaran') ?>

    <?php // echo $form->field($model, 'tekanan_darah') ?>

    <?php // echo $form->field($model, 'nadi') ?>

    <?php // echo $form->field($model, 'suhu') ?>

    <?php // echo $form->field($model, 'pernapasan') ?>

    <?php // echo $form->field($model, 'berat_badan') ?>

    <?php // echo $form->field($model, 'tinggi_badan') ?>

    <?php // echo $form->field($model, 'lila') ?>

    <?php // echo $form->field($model, 'nyeri') ?>

    <?php // echo $form->field($model, 'keterangan_lain') ?>

    <?php // echo $form->field($model, 'info_balik') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
