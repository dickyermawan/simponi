<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-11 17:37:35
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-11 17:39:24
*/

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RujukanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rujukan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tgl_masuk') ?>

    <?= $form->field($model, 'jenis') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'jk') ?>

    <?php // echo $form->field($model, 'tplahir') ?>

    <?php // echo $form->field($model, 'tglahir') ?>

    <?php // echo $form->field($model, 'umur') ?>

    <?php // echo $form->field($model, 'alamat') ?>

    <?php // echo $form->field($model, 'cara_bayar') ?>

    <?php // echo $form->field($model, 'nik') ?>

    <?php // echo $form->field($model, 'no_bpjs_jkd') ?>

    <?php // echo $form->field($model, 'asal_rujukan') ?>

    <?php // echo $form->field($model, 'tujuan_rujukan') ?>

    <?php // echo $form->field($model, 'alasan_rujukan') ?>

    <?php // echo $form->field($model, 'anamnesa') ?>

    <?php // echo $form->field($model, 'kesadaran') ?>

    <?php // echo $form->field($model, 'tekanan_darah') ?>

    <?php // echo $form->field($model, 'nadi') ?>

    <?php // echo $form->field($model, 'suhu') ?>

    <?php // echo $form->field($model, 'pernapasan') ?>

    <?php // echo $form->field($model, 'nyeri') ?>

    <?php // echo $form->field($model, 'pemeriksaan_fisik') ?>

    <?php // echo $form->field($model, 'pemeriksaan_penunjang') ?>

    <?php // echo $form->field($model, 'diagnosa') ?>

    <?php // echo $form->field($model, 'tindakan_yg_sdh_diberikan') ?>

    <?php // echo $form->field($model, 'info_balik') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
