<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-13 22:11:43
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-02 10:18:52
*/

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\Penolong;
use yii\widgets\Pjax;

?>

<div class="table-responsive tabel-monitoring">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => 'Pertama',
            'lastPageLabel'  => 'Terakhir'
        ],
        'rowOptions' => function($model) {
            if ($model->status==='Menunggu') return ['class' => 'success'];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'attribute' => 'tgl_masuk',
                'headerOptions' => ['style' => 'width:14%'],
                'value' => function($model){
                    return $model->tgl_masuk;
                    // return Yii::$app->formatter->asDate($model->tgl_masuk);
                },
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Contoh: 2018-12-23'
                 ]
            ],

            'nama',
            // 'ibu_tplahir',
            // 'ibu_tglahir',
            
            //'ibu_alamat:ntext',
            //'ibu_carabayar',
            [
                'attribute' => 'jk',
                'value' => function ($model) {
                    if($model->jk)
                        return $model->jk;
                    else
                        return '';
                },
                'format' => 'html',
            ],
            'umur',
            //'bayi_nama',
            //'bayi_tplahir',
            //'bayi_tglahir',
            //'bayi_umur',
            //'bayi_alamat:ntext',
            //'bayi_carabayar',

            [
                'attribute' => 'asal_rujukan_text',
                'header' => '<font color="#2fa4e">Asal Rujukan</font>',
                'value' => function ($model) {
                    return $model->asal_rujukan_text;
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'tujuan_rujukan_text',
                'header' => '<font color="#2fa4e">Tujuan Rujukan</font>',
                'value' => function ($model) {
                    return $model->tujuan_rujukan_text;
                },
                'format' => 'html',
            ],



            // 'asal_rujukan:ntext',
            
            // 'tujuan_rujukan',

            //'tindakan_ygdiberikan:ntext',
            'alasan_rujukan:ntext',
            'diagnosa:ntext',
            //'kesadaran',
            //'tekanan_darah',
            //'nadi',
            //'suhu',
            //'pernapasan',
            //'berat_badan',
            //'tinggi_badan',
            //'lila',
            //'nyeri',
            //'keterangan_lain:ntext',
            // 'info_balik:ntext',
            // 'status',
            // [
            //     'headerOptions' => ['class' => 'text-center'],
            //     'contentOptions' => ['class' => 'text-center'],
            //     'attribute' => 'status',
            //     'value' => function ($model) {
            //         return Penolong::label($model->status);
            //     },
            //     'format' => 'html',
            // ],


            // ['class' => 'yii\grid\ActionColumn'],
            [
                'class'    => 'yii\grid\ActionColumn',
                'header' => 'Aksi',
                'headerOptions' => ['style' => 'color:#2fa4e7; text-align:center;'],
                'contentOptions' => ['class' => 'text-center'],
                'template' => '{proses}{diterima}{tidak_diterima}',
                'visibleButtons'=>[
                    'proses'=> function($model){
                          return $model->status==='Menunggu';
                     },
                     'diterima'=> function($model){
                          return $model->status==='Diterima';
                     },
                     'tidak_diterima'=> function($model){
                          return $model->status==='Tidak Diterima';
                     },
                ],
                'buttons'  => [                    
                    'proses' => function ($url, $model) {
                        $url = Url::to(['rujukan/proses', 'id' => $model->id]);
                        return Html::a('<span class="blink_text">Proses</span>', $url, ['title' => 'update', 'class' => 'btn btn-primary btn-xs', 'style' => 'width:100%']);
                    },
                    'diterima' => function ($url, $model){
                        return Penolong::label($model->status);
                    },
                    'tidak_diterima' => function ($url, $model){
                        return Penolong::label($model->status);
                    },
                ]
            ],

        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>