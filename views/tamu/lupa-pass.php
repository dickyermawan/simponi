<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-08 00:48:32
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-04 11:36:43
*/

?>

<div class="tamu-lupa-pass" style="text-align: center; margin:50px">
	<h4 style="color:black; margin-bottom: 0px">JIKA LUPA PASSWORD DAPAT MENGHUBUNGI KE : SUPER ADMIN</h4> <br>
	 NO. HP:<br>
	 +62 853-5883-0303<br>
	 +62 852-6559-7007
	<br><br>
	<u>Terimakasih. <li class="fa fa-pencil"></li></u>
</div>